<?php

namespace Drupal\druidfire;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Schema;

class SpellBase implements SpellInterface {

  protected Schema $schema;

  public function __construct(protected Connection $database) {
    $this->schema = $this->database->schema();
  }


  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array {
    return $schema;
  }

  public function storage(array $yaml, array $args = []): array {
    return $yaml;
  }

  public function field(array $yaml, array $args = []): array {
    return $yaml;
  }

  public function formDisplay(array $yaml, $fieldName, array $args = []): array {
    return $yaml;
  }

  public function viewDisplay(array $yaml, $fieldName, array $args = []): array {
    return $yaml;
  }

}
