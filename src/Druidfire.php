<?php

namespace Drupal\druidfire;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * The main Druidfire class.
 */
class Druidfire {

  protected KeyValueStoreInterface $keyvalue;

  protected array $spells = [];

  public function __construct(protected StorageInterface $configStorage, protected EntityTypeManagerInterface $entityTypeManager, protected EntityFieldManagerInterface $entityFieldManager, KeyValueFactoryInterface $keyValueFactory, protected MessengerInterface $messenger) {
    $this->keyvalue = $keyValueFactory->get('entity.storage_schema.sql');
  }

  public function __call(string $spellName, array $arguments) {
    $entityTypeId = array_shift($arguments);
    $fieldName = array_shift($arguments);
    $optionalArguments = array_shift($arguments);
    $columnName = $this->getColumnName($entityTypeId, $fieldName, $optionalArguments['property'] ?? NULL);
    $key = "$entityTypeId.field_schema_data.$fieldName";
    $callable = [$this->spells[$spellName], 'schema'];
    $schema = $this->keyvalue->get($key, []);
    foreach (array_keys($schema) as $tableName) {
      try {
        $this->messenger->addMessage("Changing the schema of $tableName $columnName");
        $schema = $callable($schema, $tableName, $columnName, $optionalArguments);
      }
      catch (\Exception $e) {
        $this->messenger->addWarning("Schema change failed " . $e->getMessage());
      }
    }
    $this->keyvalue->set($key, $schema);
    $this->changeConfig($entityTypeId, $fieldName, $spellName, 'storage', $optionalArguments);
    $this->changeConfig($entityTypeId, $fieldName, $spellName, 'field', $optionalArguments);
    $this->entityTypeManager->clearCachedDefinitions();
  }

  /**
   * @param $entityTypeId
   * @param $fieldName
   *
   * @return string
   */
  protected function getColumnName($entityTypeId, $fieldName, $propertyName): string {
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    assert($entityStorage instanceof SqlEntityStorageInterface);
    $mapping = $entityStorage->getTableMapping();
    $definitions = $this->entityFieldManager->getFieldStorageDefinitions($entityTypeId);
    if (!isset($definitions[$fieldName]) || !$mapping instanceof DefaultTableMapping || !$entityStorage instanceof SqlContentEntityStorage) {
      throw new \InvalidArgumentException('Can only change default SQL fields.');
    }
    $definition = $definitions[$fieldName];
    return $mapping->getFieldColumnName($definition, $propertyName ?: $definition->getMainPropertyName());
  }

  /**
   * @param $entityTypeId
   * @param $fieldName
   * @param $spellName
   * @param $configType
   *   Can be storage or field.
   * @param $optionalArguments
   */
  protected function changeConfig($entityTypeId, $fieldName, $spellName, $configType, $optionalArguments) {
    $callable = [$this->spells[$spellName],  $configType];
    $configNames = $this->configStorage->listAll("field.$configType.$entityTypeId.");
    $configNames = preg_grep("/\\.$fieldName$/", $configNames);
    $this->doChangeConfig($callable, $configNames, [$optionalArguments]);
    if ($configType === 'field') {
      array_unshift($optionalArguments, $fieldName);
      $callable[1] = 'formDisplay';
      $this->doChangeConfig($callable, $configNames, [$fieldName, $optionalArguments], "core.entity_form_display.$entityTypeId.");
      $callable[1] = 'viewDisplay';
      $this->doChangeConfig($callable, $configNames, [$fieldName, $optionalArguments], "core.entity_view_display.$entityTypeId.");
    }
  }

  /**
   * @param $callable
   * @param array $configNames
   * @param array $args
   * @param string $configPrefix
   *   This will be core.entity_form_display.$entityTypeId. or
   *   core.entity_view_display.$entityTypeId.
   */
  protected function doChangeConfig($callable, array $configNames, array $args, $configPrefix = '') {
    if (!is_callable($callable)) {
      return;
    }
    foreach ($configNames as $configName) {
      if ($configPrefix) {
        // This extracts the bundle from the field config name which is
        // field(0).field(1).node(2).nodetype(3).fieldname(4)
        $actualConfigNames = $this->configStorage->listAll($configPrefix . explode('.', $configName)[3] . '.');
      }
      else {
        $actualConfigNames = [$configName];
      }
      foreach ($actualConfigNames as $actualConfigName) {
        if ($yaml = $this->configStorage->read($actualConfigName)) {
          $this->messenger->addMessage("Changing config $actualConfigName");
          $yaml = $callable($yaml, ...$args);
          $this->configStorage->write($actualConfigName, $yaml);
        }
      }
    }
  }

  /**
   * @param \Drupal\druidfire\SpellInterface $spell
   *   A spell.
   * @param $spellName
   *   The name of the spell.
   *
   * @return void
   */
  public function addSpell(SpellInterface $spell, $spellName) {
    $this->spells[$spellName] = $spell;
  }

}
