<?php

namespace Drupal\druidfire\Spells;

use Drupal\druidfire\SpellBase;

class String2Formatted extends SpellBase {

  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array {
    $spec = [
      'type' => 'text',
      'size' => 'big',
    ];
    $schema[$tableName]['fields'][$columnName] = $spec;
    $this->schema->changeField($tableName, $columnName, $columnName, $spec);
    $format = preg_replace('/_value$/', '_format', $columnName);
    $schema[$tableName]['fields'][$format] = [
      'type' => 'varchar_ascii',
      'length' => 255,
    ];
    $schema[$tableName]['indexes'][$format] = [$format];
    $this->schema->addField($tableName, $format, $schema[$tableName]['fields'][$format]);
    $this->schema->addIndex($tableName, $format, $schema[$tableName]['indexes'][$format], $schema[$tableName]);
    return $schema;
  }

  public function storage(array $yaml, array $args = []): array {
    $yaml['type'] = 'text_long';
    return $yaml;
  }

  public function field(array $yaml, array $args = []): array {
    $yaml['field_type'] = 'text_long';
    return $yaml;
  }

  public function formDisplay(array $yaml, $fieldName, array $args = []): array {
    $yaml['content'][$fieldName]['type'] = 'text_textarea';
    unset($yaml['content'][$fieldName]['settings']['size']);
    $yaml['content'][$fieldName]['settings']['rows'] = 9;
    return $yaml;
  }

}
