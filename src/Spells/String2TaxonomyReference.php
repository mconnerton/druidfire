<?php

namespace Drupal\druidfire\Spells;

use Drupal\druidfire\SpellBase;
use Drupal\taxonomy\Entity\Term;

class String2TaxonomyReference extends SpellBase {

  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array {
    $vid = $args['vid'];
    $new_column_name = preg_replace('/_value$/', '_target_id', $columnName);
    $new_column = [
      'description' => 'The ID of the target entity.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];
    $this->schema->addField($tableName, $new_column_name, $new_column);
    $schema[$tableName]['fields'][$new_column_name] = $new_column;

    // Now fill the new column.
    $escapedColumnName = $this->database->escapeField($columnName);
    // First, create the terms from values but only once.
    $subquery = $this->database->select('taxonomy_term_field_data', 't')
      ->fields('t')
      ->where("f.$escapedColumnName = t.name AND t.vid = :vid", [':vid' => $vid]);
    $query = $this->database->select($tableName, 'f');
    $query->distinct();
    $query->addField('f', $columnName, 'name');
    $query->addExpression(':vid', 'vid', [':vid' => $vid]);
    $query->notExists($subquery);
    array_map(fn($data) => Term::create($data)->save(), $query->execute()->fetchAll(\PDO::FETCH_ASSOC));
    // Second, put the term IDs in the new column.
    $update = $this->database->update($tableName);
    $update->expression($new_column_name, "(SELECT tid FROM {taxonomy_term_field_data} WHERE vid = :vid AND $escapedColumnName = name)", [':vid' => $vid]);
    $update->execute();
    $this->schema->dropField($tableName, $columnName);
    return $schema;
  }

  public function storage(array $yaml, array $args = []): array {
    $yaml['type'] = 'entity_reference';
    $yaml['settings'] = [];
    $yaml['settings']['target_type'] = 'taxonomy_term';
    return $yaml;
  }

  public function field(array $yaml, array $args = []): array {
    $vid = $args['vid'];
    $yaml['field_type'] = 'entity_reference';
    $yaml['settings'] = [];
    $yaml['settings']['handler'] = 'default:taxonomy_term';
    $yaml['settings']['handler_settings']['target_bundles'][$vid] = $vid;
    $yaml['settings']['handler_settings']['auto_create'] = FALSE;
    return $yaml;
  }

  public function formDisplay(array $yaml, $fieldName, array $args = []): array {
    $yaml['content'][$fieldName]['type'] = 'entity_reference_label';
    $yaml['content'][$fieldName]['settings'] = [];
    $yaml['content'][$fieldName]['settings']['link'] = FALSE;
    return $yaml;
  }

  public function viewDisplay(array $yaml, $fieldName, array $args = []): array {
    $yaml['content'][$fieldName]['type'] = 'options_select';
    $yaml['content'][$fieldName]['settings'] = [];
    return $yaml;
  }

}
