<?php

namespace Drupal\druidfire;

interface SpellInterface {

  /**
   * Change the table of a field table.
   *
   * @param array $schema
   *   The current table schema.
   * @param string $tableName
   *   The name of the field table. For example, node__body.
   * @param string $columnName
   *   The column of the field property being changed. Usually the main
   *   property like value. For example, body_value.
   * @param array $args
   *   Any additional arguments this spell needs.
   *
   * @return array
   *   The changed table schema.
   */
  public function schema(array $schema, string $tableName, string $columnName, array $args = []): array;

  /**
   * Change the field.storage.$entityTypeId.$fieldName config object.
   *
   * @param array $yaml
   *   The field storage config array.
   * @param array $args
   *   Any additional arguments this spell needs.
   *
   * @return array
   *   The changed field storage config array.
   */
  public function storage(array $yaml, array $args = []): array;

  /**
   * Change a field.field.$entityTypeId.$bundle.$fieldName config object.
   *
   * @param array $yaml
   *   The field config array.
   * @param array $args
   *   Any additional arguments this spell needs.
   *
   * @return array
   *   The changed field config array.
   */
  public function field(array $yaml, array $args = []): array;

  /**
   * Change a core.entity_form_display.$entityTypeId.$displayMode config object.
   *
   * @param array $yaml
   *   The form display config array.
   * @param $fieldName
   *   The name of the field. Most of the time only
   *   $yaml['content'][$fieldName] will need changing.
   * @param array $args
   *   Any additional arguments this spell needs.
   *
   * @return array
   *   The updated form display config array.
   */
  public function formDisplay(array $yaml, $fieldName, array $args = []): array;

  /**
   * Change a core.entity_view_display.$entityTypeId.$displayMode config object.
   *
   * @param array $yaml
   *   The view display config array.
   * @param $fieldName
   *   The name of the field. Most of the time only
   *   $yaml['content'][$fieldName] will need changing.
   * @param array $args
   *   Any additional arguments this spell needs.
   *
   * @return array
   *   The updated view display config array.
   */
  public function viewDisplay(array $yaml, $fieldName, array $args = []): array;

}
